/**
 * Created by ju on 21/12/16.
 */

getModules.DragAndDropModule = function () {
    var exports = {};

    class Draggable
    {
        constructor(selectable,movable, offsetX, offsetY)
        {
            this.selectableElt_         = document.querySelector(selectable);
            this.movable_               = document.querySelector(movable);
            this.isOnDragging_          = false;
            this.offsetX                = offsetX;
            this.offsetY                = offsetY;
            this.interval               = undefined;

            // Events Management
            this.selectableElt_.addEventListener('mousedown',(function (e) {
                if(e.button == 0)
                {
                    this.isOnDragging_ = true;
                    console.log('on');
                }
            }).bind(this));

            document.addEventListener('mousemove',(function (e) {
                if(this.isOnDragging_)
                {
                    this.movable_.style.top = e.clientY-this.offsetY+'px';
                    this.movable_.style.left = e.clientX-this.offsetX+'px';
                }
            }).bind(this));

            this.selectableElt_.onmouseup = (function (e) {
                if(e.button == 0)
                {
                    this.isOnDragging_ = false;
                    console.log('off');
                }
            }).bind(this);
        }
    }

    exports.DraggableClass = Draggable;
    return exports;
};