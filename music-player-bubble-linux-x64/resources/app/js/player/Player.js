/**
 * Created by ju on 16/12/16.
 */



getModules.PlayerModule = function () {
    let exports = {};
    let ScrollingTextClass = getModules.ScrollingTextModule().ScrollingTextClass;
    class Player
    {
        // Constructor
        constructor(ipcRenderer)
        {
            this._currentSongId             = 0;//Math.floor(Math.random()*songList.length);
            this.totalTime_                 = 0;
            this.totalTimeFormat_           = '00:00:00';
            this.state_                     = false;
            this.timeBar_                   = document.querySelector('#time_bar_current');
            this.timeText_                  = document.querySelector('#time_text');
            this.titleText_                 = new ScrollingTextClass(document.querySelector('#title_text'),20);
            this.buttonShuffle_             = document.querySelector('#shuffle_button');
            this.buttonPlayPause_           = document.querySelector('#play_pause_button');
            this.buttonNext_                = document.querySelector('#next_button');
            this.buttonIncrease_            = document.querySelector('#increase_volume_button');
            this.buttonDecrease_            = document.querySelector('#decrease_volume_button');
            this.playerCommandsContour_     = {domElt: document.querySelector('#player_commands_contour'),state:true};
            this.bubble_                    = document.querySelector('#player_bubble');
            this.playerCommandsContourSize_ = this.playerCommandsContour_.domElt.style.height;
            this.hiddenable_                = true;

            this.configureCommunication(ipcRenderer);
            this.configureEvents();
        };
        // Methods
        configureCommunication(ipcRenderer)
        {
            this.buttonShuffle_.addEventListener('click',function () {
                ipcRenderer.send('player_shuffle');
            });

            this.buttonPlayPause_.addEventListener('click',function () {
                ipcRenderer.send('player_play_pause');
            });

            this.buttonNext_.addEventListener('click',function () {
                ipcRenderer.send('player_next');
            });

            this.buttonIncrease_.addEventListener('click',function () {
                ipcRenderer.send('player_volume_up');
            });

            this.buttonDecrease_.addEventListener('click',function () {
                ipcRenderer.send('player_volume_down');
            });
        }

        configureEvents()
        {
            this.bubble_.addEventListener('click', (function (e) {
                console.log('click');
                if(e.button == 0 && this.hiddenable_)
                {
                    if(this.playerCommandsContour_.state)
                    {
                        this.playerCommandsContour_.domElt.style.height = '0px';
                        this.playerCommandsContour_.domElt.style.width = '0px';
                        this.playerCommandsContour_.state = false;
                    }
                    else
                    {
                        this.playerCommandsContour_.domElt.style.height = this.playerCommandsContourSize_;
                        this.playerCommandsContour_.domElt.style.width = '100%';
                        this.playerCommandsContour_.state = true;
                    }
                }
            }).bind(this));
        }

        pause()
        {

            this.buttonPlayPause_.backgroundImage = "url('/images/player/play.png')";
            // this._audioElement.pause();
        }

        play()
        {

            this.buttonPlayPause_.backgroundImage = "url('/images/player/break.png')";
            // this._audioElement.play();
        }

        updateMetaData()
        {
            // this.titleText_.setText(this._audioElement.src.split('/').reverse()[0].split('.')[0]);
            // let time = this._audioElement.duration;
            // let sec = Math.floor(time)%60;
            // let min = Math.floor(time%3600/60);
            // let hour = Math.floor(time/3600);
            // this.totalTimeFormat_ = ((hour<10) ? '0'+hour:hour)+':'+((min<10) ? '0'+min:min)+':'+((sec<10) ? '0'+sec:sec);
        }
    }

    exports.PlayerClass = Player;
    return exports;
};