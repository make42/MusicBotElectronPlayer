/**
 * Created by ju on 23/12/16.
 */

getModules.ScrollingTextModule = function () {
    let exports = {};

    class ScrollingText
    {
        constructor(TextContainer,offsetMin)
        {
            // Attributes
            this.text_              = undefined;
            this.elt_               = undefined;
            this.copie_             = document.createElement('span');
            this.scrollingInterval_ = undefined;
            this.offset_            = undefined;
            this.offsetMin_         = offsetMin;


            let text = TextContainer.textContent;
            TextContainer.textContent = "";
            TextContainer.style.display = 'flex';
            let containerWidth = TextContainer.offsetWidth+20;
            let elt     = document.createElement('span');
            elt.textContent = text;
            elt.style.position = 'relative';
            elt.style.top = '0px';
            elt.style.left = '0px';
            let copie   = elt.cloneNode(true);
            let textWidth = elt.offsetWidth;
            let diffWidth = containerWidth-textWidth;
            this.offset_ = Math.max(diffWidth,offsetMin);

            this.elt_ = elt;
            this.copie_ = copie;

            TextContainer.appendChild(this.elt_);
            TextContainer.appendChild(this.copie_);

            this.elt_.style.top = 0;
            this.elt_.style.left = 0;
            this.copie_.style.top = 0;
            this.copie_.style.left = this.offset_+'px';
            this.scrollingInterval_ = setInterval((function () {
                let currentLeft = parseInt(this.elt_.style.left);
                let length = this.elt_.offsetWidth+this.offset_;
                this.elt_.style.left = (currentLeft+length<0)? 0 : currentLeft-1+'px';
                this.copie_.style.left = (currentLeft+length<0)? 0 : currentLeft+this.offset_-1+'px';
            }).bind(this),20);
        }

        setText(text)
        {
            // clearInterval(this.scrollingInterval_);
            this.elt_.textContent = text;
            this.copie_.textContent = text;
            let textWidth = this.elt_.offsetWidth;
            let diffWidth = this.elt_.parentNode.offsetWidth+20-textWidth;
            console.log(diffWidth);
            this.offset_ = Math.max(diffWidth,this.offsetMin_);

            this.elt_.style.top = 0;
            this.elt_.style.left = 0;
            this.copie_.style.top = 0;
            this.copie_.style.left = this.offset_+'px';

        }
    }

    exports.ScrollingTextClass = ScrollingText;
    return exports;
};